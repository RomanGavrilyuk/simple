//Считать с клавиатуры число. Если число положительное, вывести на экран «yes», иначе вывести «no».

#include <stdio.h>

int main() {
	int number1;

	printf("type digit: \n");
	scanf("%d", &number1);
	if (number1 >= 0) {
		printf("yes \n");
	} else {
		printf("no \n");
	}
	
	return 0;
}