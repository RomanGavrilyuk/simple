// Считать с клавиатуры целое положительное число.
// Вывести на экран наибольший его нетривиальный делитель. В случае неопределенности вывести 0.

#include <stdio.h>

int main() {
	int dividend;
	int divisor;

	printf("type dividend:\n");
	scanf("%d", &dividend);
	
	for(divisor = dividend/2; divisor > 1;divisor--) {
		if (dividend % divisor == 0) {
			printf("%d\n", divisor);
			return 0;
		}
	}
	printf("0\n");

	return 0;
}