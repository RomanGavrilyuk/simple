// Считать с клавиатуры целые числа min и max. Вывести в строку кубы всех нечетных чисел в промежутке от min до max включительно. 
// В указаном промежутке гарантированно существует минимум одно нечетное число.

// Пример ввода
// 0 7
// Пример вывода
// 1 27 125 343


#include <stdio.h>

int main() {
	int min;
	int max;

	printf("type min and max numbers:\n");
	scanf("%d %d", &min, &max);
	
	for(int num = min; num <= max; num++ ) {
		if(num % 2 == 1) {
			printf("%d ", num * num * num);
		}
	}
	
	printf("\n");

	return 0;
}