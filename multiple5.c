// Считать с клавиатуры целое положительное число.
// Вывести в строку все числа, кратные 5, от нуля до указанного числа включительно.

#include <stdio.h>


#define div 5

int main() {
	int digit;

	printf("type digit: \n", &digit);
	scanf("%d", &digit);
    
    digit -= digit % div;
    
    for ( int i = 0; i < digit; i += div) {
        printf("%d ", i);
    }
    printf("%d\n", digit);
    
    return 0;
}