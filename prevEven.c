//Считать целое число. Вывести на экран максимальное четное число, не большее введенного.

#include <stdio.h>

int main() {
	int number1;

	printf("type number: \n");
	scanf("%d", &number1);

	if (number1 % 2 == 0) {
		printf(" number is pair and = %d\n", number1);
	} else {
		printf("number is not pair. Bigger pair number = %d\n", number1 - 1 );
	}
	
	return 0;
}