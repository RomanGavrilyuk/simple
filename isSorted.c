// Считать с клавиатуры числовую последовательность - 5 целых чисел. Проверить, упорядочена ли последовательность по неубыванию.
// Если упорядочена вывести на экран «yes», в протиивном случае вывести «no». Задача решается без использования циклов.

#include <stdio.h>

int main() {
	int digit1;
	int digit2;
	int digit3;
	int digit4;
	int digit5;

	printf("type 5 digits:\n");
	scanf("%d %d %d %d %d", &digit1, &digit2, &digit3, &digit4, &digit5);
	if(digit1 < digit2, digit2 < digit3, digit3 < digit4, digit4 < digit5) {
		
		printf("yes\n");
	
		} else{
			printf("no\n");
		}

	return 0;
}