// Вывести числовой квадрат заданного размера.
// Выведенные числа начинаются с единицы и постоянно увеличиваются.
// В каждой строке числа разделены пробелами.
// Порядок строк обратный.
// Размер считать с клавиатуры.

// Пример ввода
// 3
// Пример вывода
// 7 8 9 
// 4 5 6
// 1 2 3

#include <stdio.h>
int main() {
	int size;
	int counter;

	printf("input size of the square:\n");
	scanf("%d", &size);

	counter = size * size;

	for (int rows = 1; rows <= size; rows++) {
			for (int cols = 1; cols <= size; cols++) {
				printf("%d ", (counter - ((size * rows) - cols)));
				}
		printf("\n");
	}

	return 0;

}