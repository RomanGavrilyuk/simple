// Считать min, max. Вывести в столбик кратные пяти числа от min до max включительно.

// Пример ввода
// 0 15
// Пример вывода
// 0
// 5
// 10
// 15

#include <stdio.h>

int main() {
	int min;
	int max;

    scanf("%d %d", &min, &max);

    for (min = 0; min <= max; min++) {
        if (min % 5 == 0) {
        	printf("%d\n", min);
    	}
	}

    return 0;
}