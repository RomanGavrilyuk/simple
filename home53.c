// Написать функцию:
// int isPrime(int n)
// Проверить, является ли данное число простым.

#include <stdio.h>
int isPrime(int x) {
    for ( int i = 2; i <= x/2; i++ ) {
        if (x % i == 0) {
        	return 1;
        }
        
   	}
    return 0;
}

int main() {

	int a;
	
	scanf("%d", &a);

	if (isPrime(a)) {
		printf("NO\n");
	} else {
		printf("YES\n");
	}

	return 0;
}