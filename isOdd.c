//Считать с клавиатуры целое число.
//Если число нечётное, вывести на экран yes, в противном случае вывести no.

//Считать с клавиатуры целое число.
//Если число чётное, вывести на экран yes, в противном случае вывести no.

#include <stdio.h>

int main() {
	int number1;

	printf("type number: \n");
	scanf("%d", &number1);

	if (number1 % 2 == 0) {
		printf("no \n");
	} else {
		printf("yes \n");
	}
	
	return 0;
}