// Считать с клавиатуры целые числа min и max. Вывести в строку квадраты всех четных чисел в промежутке от min до max включительно.
// В указанном промежутке гарантированно существует как минимум 1 парное число.

#include <stdio.h>

int main() {
	int min;
	int max;

	printf("type min and max numbers:\n");
	scanf("%d %d", &min, &max);
	
	for(int num = min; num <= max; num++ ) {
		if(num % 2 == 0) {
			printf("%d ", num * num);
		}
	}
	
	printf("\n");

	return 0;
}