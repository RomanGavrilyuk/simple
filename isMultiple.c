//Считать с клавиатуры два целых числа, причем второе из них не равно нулю. 
//Если первое кратно второму, вывести на экран yes, иначе вывести no.

#include <stdio.h>

int main() {
	int number1;
	int number2;
	int max;

	printf("type numbers: \n");
	scanf("%d %d", &number1, &number2);
	if (number1 % number2 == 0) {
	
		printf("yes \n");
	}else {
		printf("no \n");
	}
	
	return 0;
}