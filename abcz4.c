// Считать с клавиатуры два целых числа.
// Программа должна произвести только одну из следующих операций:
// - Если первое число больше второго и второе число больше нуля, вывести alpha
// - Если первое число меньше нуля и второе число равно нулю, вывести bravo
// - Если любое из чисел равно нулю, вывести charlie
// - Если не сработало ничего из вышеуказанного, вывести zulu

#include <stdio.h>

int main() {
	int number1;
	int number2;

	printf("type 2 digits: \n");
	scanf("%d %d", &number1, &number2);
	
	 if (number2 > 0 && number1 > number2) {
		printf("alpha \n");
	} else  if (number1 < 0 && number2 == 0) {
		printf("bravo \n");
	} else  if (number1 == 0 || number2 == 0) {
		printf("charlie \n");
	} else { 
	printf("zulu\n");
	}

	return 0;
}