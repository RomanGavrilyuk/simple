// Считать с клавиатуры 2 целых положительных числа, количество забитых голов хозяевами 
// и гостями на футбольном матче. Вывести на экран результат матча «Home team wins», «Away team wins», «Draw».

#include <stdio.h>

int main() {
	int a;
	int b;

	printf("type 2 digits:\n");
	scanf("%d %d", &a, &b);
	if(a > b) {
		printf("Home team wins \n");
		} else if(a == b) {
			printf("Draw \n");
		} else{
			printf("Away team wins \n");
		}

	return 0;
}