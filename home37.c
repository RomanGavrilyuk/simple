// Считать min, max. Вывести в столбик кратные тройке числа от min до max включительно.

// Пример ввода
// 0 9
// Пример вывода
// 0
// 3
// 6
// 9

#include <stdio.h>

int main() {
	int min;
	int max;

    scanf("%d %d", &min, &max);

    for (min = 0; min <= max; min++) {
        if (min % 3 == 0) {
        	printf("%d\n", min);
    	}
	}

    return 0;
}