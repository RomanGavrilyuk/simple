//Считать с клавиатуры 2 целых числа. Если они равны вывести на экран «yes», в противном случае вывести «no».

#include <stdio.h>

int main() {
	int number1;
	int number2;

	printf("type numbers: \n");
	scanf("%d %d", &number1, &number2);
	if (number1 == number2) {
	
		printf("yes \n");
	}else {
		printf("no \n");
	}
	
	return 0;
}