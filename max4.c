//Считать с клавиатуры четыре целых числа и вывести большее из них.
//В случае равенства выводить любое.
//Данная задача решается без массивов, циклов и функций.

#include <stdio.h>

int main() {
	int number1;
	int number2;
	int number3;
	int number4;
	int max;

	printf("type numbers: \n");
	scanf("%d %d %d %d", &number1, &number2, &number3, &number4);
	
	max = number1 > number2 ? number1 : number2 ;
	max = max > number3 ? max : number3 ;
	max = max > number4 ? max : number4 ;

	printf("biggest number = %d\n", max);
	
	return 0;
}