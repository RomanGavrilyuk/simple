// 35. prevMultiple

// [12:27] 
// Считать с клавиатуры два целых числа - делимое и делитель, причем делитель не равен нулю.
// Вывести на экран наибольшее число, кратное делителю, не превышающее делимое.

// Пример ввода
// 15 6
// Пример вывода
// 12

#include <stdio.h>

int main() {
    int dividend;
    int divisor;
    int d = 1;
    
    scanf("%d %d", &dividend, &divisor);
        
    for ( ; divisor * d <= dividend; d++) {
    }
    printf("%d\n", divisor * (d - 1));

    return 0;
}