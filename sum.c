// Написать функцию
// int sum(int x, int y)

#include <stdio.h>

int getSum(int var1, int var2, int var3, int var4) {
	return var1 + var2 + var3 + var4;
}

int main() {
	int a;
	int b;
	int c;
	int d;
	int sum;

	scanf("%d %d %d %d", &a, &b, &c, &d);

	sum = getSum(a, b, c, d);

	printf("sum= %d\n", sum);

	return 0;
}