// Считать с клавиатуры целые числа min, max и положительный делитель. Вывести в столбик числа, кратные делителю, от min до max включительно.

// Пример ввода
// 0 8 2
// Пример вывода
// 0
// 2
// 4
// 6
// 8

#include <stdio.h>

int main() {
	int min;
	int max;
	int div;

    scanf("%d %d %d", &min, &max, &div);

    for (min = 0; min <= max; min++) {
        if (min % div == 0) {
        	printf("%d\n", min);
    	}
	}

    return 0;
}