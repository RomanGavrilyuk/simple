// Считать с клавиатуры два целых числа.
// Вывести результат логических действий И и ИЛИ.

#include <stdio.h>

int main() {
	int number1;
	int number2;

	printf("type 2 digits: \n");
	scanf("%d %d", &number1, &number2);
	
	printf("And: %d\n", number1 && number2);
	printf("OR: %d\n", number1 || number2);
	
	return 0;
}