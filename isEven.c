//Считать с клавиатуры целое число.
//Если число чётное, вывести на экран yes, в противном случае вывести no.

#include <stdio.h>

int main() {
	int number1;

	printf("type number: \n");
	scanf("%d", &number1);

	if (number1 % 2 == 0) {
		printf("yes \n");
	} else {
		printf("no \n");
	}
	
	return 0;
}