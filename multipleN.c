// Считать с клавиатуры целое неотрицательное число-максимум.
// Считать с клавиатуры целый, не равный нулю делитель.
// Вывести в строку числа от нуля до указанного максимума включительно, кратные заданному делителю.

#include <stdio.h>

int main() {
    int dividend;
    int divisor;
    int d = 0;
    
    scanf("%d %d", &dividend, &divisor);
        
    for ( ; divisor * d <= dividend; d++) {
        printf("%d ", divisor * d);
    }

    printf("\n");

    return 0;
}