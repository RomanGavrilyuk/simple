// 34. nextMultiple

// [12:26] 
// Считать с клавиатуры два целых числа - делимое и делитель, причем делитель не равен нулю.
// Вывести на экран наименьшее число, кратное делителю, не меньшее делимого.
// Пример ввода
// 15 6
// Пример вывода
// 18

#include <stdio.h>

int main() {
    int dividend;
    int divisor;
    int d = 1;
    
    scanf("%d %d", &dividend, &divisor);
        
    for ( ; divisor * d <= dividend; d++) {
    }
    printf("%d\n", divisor * d);

    return 0;
}