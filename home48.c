// Считать с клавиатуры положительную длину числовой последовательности и саму последовательность.
// Проверить, упорядочена ли последовательность по неубыванию. Если упорядочена вывести на экран «yes», в противном случае вывести «no».

// Пример ввода
// 5
// -20 -15 0 12 15
// Пример вывода
// yes

#include <stdio.h>

int main() {
    int count, current, max;
    
    scanf("%d %d", &count, &max);
    
    for ( count = count - 1; 0 < count; count-- ) {
        scanf("%d", &current);
        if ( max < current ) {
            max = current;
        } else if (max > current) {
        	printf("no\n");
        	return 0;
        }
    }
    	printf("yes\n");
    
    return 0;
}