//Считать с клавиатуры 2 целых положительных числа - скорость движения автомобиля и ограничение по скорости. Проверить, нарушает ли
//водитель автомобиля правило ограничения скорости. Если нарушает, вывести в консоль "violation", если не нарушает — вывести «ok».


#include <stdio.h>

int main() {
	int speed;
	int speed_limit;

	printf("type speed and speed limit: \n");
	scanf("%d %d", &speed, &speed_limit);
	if (speed > speed_limit) {
	
		printf("violation \n");
	}else {
		printf("ok \n");
	}
	
	return 0;
}