// Считать min, max. Вывести в столбик чётные числа от min до max включительно.

// Пример ввода
// 0 8
// Пример вывода
// 0
// 2
// 4
// 6
// 8

#include <stdio.h>

int main() {
	int min;
	int max;

    scanf("%d %d", &min, &max);

    for (min = 0; min <= max; min++) {
        if (min % 2 == 0) {
        	printf("%d\n", min);
    	}
	}

    return 0;
}