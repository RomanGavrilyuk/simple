// Вывести числовой квадрат заданного размера.
// В каждой строке числа идут с единицы через пробел.
// Размер считать с клавиатуры.

// Пример ввода
// 2
// Пример вывода
// 1 2
// 1 2


#include <stdio.h>
int main() {
	int size;

	printf("input size of the square:\n");
	scanf("%d", &size);

	for (int rows = 1, cols = 1; rows <= size; rows++) {
		for (int cols = 1; cols <= size; cols++) {
				printf("%d ", cols);
		}
		printf("\n");
	}

	return 0;

}