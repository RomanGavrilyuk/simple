// Считать с клавиатуры целое число.
// - Если число четное, вывести alpha
// - Если число кратно трем, вывести bravo
// - Если число кратно пяти, вывести charlie
// - Если число не кратно ни одному из них, вывести zulu

#include <stdio.h>

int main() {
	int number1;

	printf("type number: \n");
	scanf("%d", &number1);
	
	 if (number1 % 2 == 0) {
		printf("alpha \n");
	} else  if (number1 % 3 == 0) {
		printf("bravo \n");
	} else  if (number1 % 5 == 0) {
		printf("charlie \n");
	} else { 
	printf("zulu\n");
	}
	return 0;
}