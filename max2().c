// Написать функцию
// int max2(int x, int y)

#include <stdio.h>

int max2(int a, int b) {
	if (a > b) {
		return a;
	} else {
		return b;
	}
}

int main() {
	int a;
	int b;
	int max;

	printf("type 2 digits:\n");
	scanf("%d %d", &a, &b);
	
	max = max2(a, b);
	printf("%d\n", max);

	return 0;
}