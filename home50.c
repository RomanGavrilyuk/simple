// Вывести числовой квадрат заданного размера.
// Выведенные числа начинаются с единицы и постоянно увеличиваются.
// В каждой строке числа разделены пробелами.
// Размер считать с клавиатуры.

// Пример ввода
// 2
// Пример вывода
// 1 2
// 3 4

#include <stdio.h>
int main() {
	int size;
	int counter = 0;

	printf("input size of the square:\n");
	scanf("%d", &size);

	for (int rows = 1, cols = 1; rows <= size; rows++) {
		for (int cols = 1; cols <= size; cols++) {
				printf("%d ", counter += 1);
		}
		printf("\n");
	}

	return 0;

}