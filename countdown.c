// Считать с клавиатуры целое неотрицательное число.
// Вывести в строку убывающий ряд чисел от заданного до нуля.

// Пример ввода
// 7
// Пример вывода
// 7 6 5 4 3 2 1 0

#include <stdio.h>

int main() {
	int limit;

	printf("type max number: \n", &limit);
	scanf("%d", &limit);
	
	for(int num = limit; num > 0; num--) {
		printf("%d ", num);
	}
		printf("%d\n", 0);
	return 0;
}