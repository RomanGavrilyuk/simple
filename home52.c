// Вычислить и вывести факториал считанного с клавиатуры целого числа.
// В случае неопределенности ответа вывести -1.
// Для решения пользоваться циклами.

// Пример ввода
// 5
// Пример вывода
// 120

#include <stdio.h>
int main() {
	int fact;
	int f = 1;

	printf("input digit:\n");
	scanf("%d", &fact);

	if (fact <= 0) {
		printf("-1\n");
		return 0;
	} else for (int i = 1; i <= fact ; i++) {
		f *= i;
		}
		printf("FACTORIAL = %d\n", f);

	return 0;

}