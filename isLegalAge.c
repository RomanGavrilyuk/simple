//Считать с клавиатуры положительное число — возвраст человека. Если человек достиг совершеннолетия (21 год), вывести на экран «yes»,
//в противном случае вывести «no».

#include <stdio.h>

int main() {
	int number1;

	printf("type age: \n");
	scanf("%d", &number1);
	if (number1 >= 21) {
		printf("yes \n");
	} else {
		printf("no \n");
	}
	
	return 0;
}