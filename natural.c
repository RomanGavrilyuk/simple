//Считать с клавиатуры целое положительное число.
//Вывести в строку через пробел по порядку все натуральные числа, не превышающие заданное.

#include <stdio.h>

int main() {
	int limit;
	int num;

	printf("type max number: \n", &limit);
	scanf("%d", &limit);
	
	for(num = 0; num <= limit; num++) {
		printf("%d ", num);
	}
	printf("\n");

	return 0;
}