// Считать с клавиатуры целое основание и целый неотрицательный показатель. Вывести через пробел степени числа основания от нулевой до заданной.

// Пример ввода
// 2 3
// Пример вывода
// 1 2 4 8

#include <stdio.h>
#include <math.h>

int main() {
	int digit;
	int step;

	printf("TYPE DIGIT AND STEP:\n");
    scanf("%d %d", &digit, &step);

    for (int i = 0; i <= step ; i++) {
    	printf("%f\n", pow (digit, i));
    }

    return 0;
}