// Считать с клавиатуры два целых положительных числа - количество одноклеточных организмов в чашке Петри
// и количество часов - время проведения эксперимента. Каждый час, каждая клетка делится на 2 клетки.
// Вывести на экран количество клеток в чашке петри на каждый час эксперимента.

// Пример ввода
// 5 2
// Пример вывода
// 1h => 10 amoeba(s)
// 2h => 20 amoeba(s)

#include <stdio.h>
#define MULT 2

int amoeba(int init, int time) {
	if(time > 0) {
		return MULT * amoeba(init, time - 1);
	}
	return init;
}

int main() {
    int init;
    int time;
    
    scanf("%d %d", &init, &time);
    printf("%d\n", amoeba(init, time));

    return 0;
}