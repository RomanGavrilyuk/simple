// Считать с клавиатуры три целых и не равных нулю числа.
// Программа должна произвести только одну из следующих операций:
// - Если сумма первого и второго чисел больше третьего числа, вывести alpha
// - Если первое число меньше разности второго и третьего чисел, вывести bravo
// - Если второе число кратно третьему, вывести charlie
// - Если не сработало ничего из вышеуказанного, вывести zulu

#include <stdio.h>

int main() {
	int number1;
	int number2;
	int number3;

	printf("type 3 numbers: \n");
	scanf("%d %d %d", &number1, &number2, &number3);
	
	 if (number1 + number2 > number3) {
		printf("alpha \n");
	} else  if (number1 < (number2 - number3)) {
		printf("bravo \n");
	} else  if (number2 % number3 == 0) {
		printf("charlie \n");
	} else { 
	printf("zulu\n");
	}
	return 0;
}